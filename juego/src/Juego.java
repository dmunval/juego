import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Juego  extends JPanel {
		public Obj1 o1 = new Obj1(this);
		public Obj2 o2 = new Obj2(this);

		public Juego() {
			addKeyListener(new KeyListener() {
				@Override
				public void keyTyped(KeyEvent e) {
				}

				@Override
				public void keyReleased(KeyEvent e) {
				}

				@Override
				public void keyPressed(KeyEvent e) {
					o1.teclaPresionada(e);
				}
			});
			setFocusable(true);
		}
		
		private void recalculaPosiciones() {
			o1.recalculaPosicion();
			o2.recalculaPosicion();
		}
		
		@Override
		public void paint(Graphics g) {
			super.paint(g);
			o1.repinta(g);
			o2.repinta(g);
		}

		public static void main(String[] args) throws InterruptedException {
			JFrame frame = new JFrame("Titulo");
			Juego game = new Juego();
			frame.add(game);
			frame.setSize(500, 600);
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			while (true) {
				game.recalculaPosiciones();
				game.repaint();
				Thread.sleep(10);
			}
		}
}