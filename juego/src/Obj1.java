import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;


public class Obj1 {
	private int x=500;
	private int y=1;
	private int dir_x=-1;
	private int dir_y=1;
	private static final int WITH = 30;
	private static final int HEIGH = 30;	
	private Juego game;
	
	public Obj1(Juego game) {
		this.game= game;
	}
	
	public void teclaPresionada(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_LEFT)
			dir_x = -1;
		if (e.getKeyCode() == KeyEvent.VK_RIGHT)
			dir_x = 1;
	}
	
	public void recalculaPosicion() {
		// recalcula posici�n en base a entradas y parametros
		this.x=this.x+dir_x;
		this.y=this.y+dir_y;	
		if (colision()) {
			dir_y = -1;
		}
	}

	public void repinta(Graphics g) {
		// Pinta el elemento en base a su posici�n, y otros parametros
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.fillOval(this.x, this.y, 30, 30);
	}
	
	private boolean colision() {
		// indica que el cuadro de o2 intersecta con el mio
		return this.game.o2.getBounds().intersects(getBounds());
	}
	
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, this.WITH, this.HEIGH);
	}
}
